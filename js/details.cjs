window.addEventListener("DOMContentLoaded", (event) => {
  console.log("Dom content Loaded");

  displayDetails();

  document.querySelector("#theme-btn").addEventListener("click", changetheme);

  document
    .querySelector("#border-countries")
    .addEventListener("click", chooseButtonCountry);
});

function chooseButtonCountry(event) {
  sessionStorage.setItem( 'showCountryDetail',`${event.target.textContent}`);
}

function changetheme() {
  if (document.querySelector("#theme-mode").textContent === "Dark Mode") {
    document.querySelector("#theme-mode").textContent = "Light Mode";
    var list = Array.from(document.querySelectorAll(".theme-elements"));
    list.forEach((item) => {
      item.style.backgroundColor = "hsl(209, 23%, 22%)";
      item.style.color = "hsl(0, 0%, 100%)";
    });
    document.querySelector("body").style.backgroundColor = "hsl(207, 26%, 17%)";
    document.querySelector("body").style.color = "hsl(0, 0%, 100%)"
  } else {
    document.querySelector("#theme-mode").textContent = "Dark Mode";
    var list = Array.from(document.querySelectorAll(".theme-elements"));
    list.forEach((item) => {
      item.style.backgroundColor = "hsl(0, 0%, 100%)";
      item.style.color = "hsl(200, 15%, 8%)";
    });
    document.querySelector("body").style.backgroundColor = "hsl(0, 0%, 98%)";
    document.querySelector("body").style.color = "hsl(200, 15%, 8%)"
  }
}

function displayDetails() {
  const countryName = sessionStorage.getItem("showCountryDetail");
  const countriesData = JSON.parse(sessionStorage.getItem("countriesData"));
  const country = countriesData.filter(
    (country) => country.name === countryName
  )[0];
  const countryCard = document.querySelector("#country-card");
  countryCard.querySelector("#country-flag img").src = country.flag;
  countryCard.querySelector("#country-name").textContent = country.name;
  countryCard.querySelector("#native-name").textContent = country.nativeInfo;
  countryCard.querySelector("#population").textContent =
    country.population.toLocaleString("en-US");
  countryCard.querySelector("#region").textContent = country.region;
  countryCard.querySelector("#sub-region").textContent = country.subregion;
  countryCard.querySelector("#capital").textContent = country.capital;
  countryCard.querySelector("#tld").textContent = country.tld;
  countryCard.querySelector("#currencies").textContent = country.currencies;
  countryCard.querySelector("#languages").textContent = country.languages;

  const borderCountries = countryCard.querySelector("#border-countries");
  if (country.borderCountries.length > 0) {
    country.borderCountries.forEach((data) => {
      const button = document.createElement("button");
      button.setAttribute("onclick","window.location.href='details.html'")
      button.setAttribute("class", "border-country-button theme-elements");
      button.textContent = data;
      borderCountries.appendChild(button);
    });
  } else {
    const button = document.createElement("button");
    button.setAttribute("class", "border-country-button theme-elements");
    button.textContent = "No Bordering Countries";
    borderCountries.appendChild(button);
  }
}
