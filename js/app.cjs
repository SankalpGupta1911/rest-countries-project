window.addEventListener("DOMContentLoaded", (event) => {
  console.log("Dom content Loaded");
  server();
  document.querySelector("#theme-btn").addEventListener("click", changetheme);

  document
    .getElementById("search-input")
    .addEventListener("keyup", searchCountry);

  document
    .getElementById("filter-bar-select")
    .addEventListener("click", filterCards);

  document
    .querySelector("#cards-section")
    .addEventListener("click", showCardDetails);
});

function showCardDetails(event) {
  let div = event.target;

  let element = div.parentNode.closest(".card-elements");

  sessionStorage.setItem(
    "showCountryDetail",
    `${element.querySelector(".country-name").textContent}`
  );
  return;
}

function filterCards(event) {
  let filterValue = event.target.value;
  let searchValue = document.getElementById("search-input").value.toLowerCase();
  console.log(searchValue);
  if (filterValue === sessionStorage.getItem("previousFilterValue")) {
    return;
  }
  if (filterValue) {
    sessionStorage.setItem("previousFilterValue", `${filterValue}`);
  } else {
    sessionStorage.setItem("previousFilterValue", "");
  }

  console.log(filterValue);
  let allCards = Array.from(document.querySelectorAll(".card-body"));

  if (filterValue === "All") {
    if (searchValue) {
      allCards.forEach((card) => {
        let countryName = card
          .querySelector(".country-name")
          .textContent.toLowerCase();
        if (countryName.includes(searchValue)) {
          card.style.display = "block";
        } else {
          card.style.display = "none";
        }
      });
      return;
    } else {
      allCards.forEach((card) => {
        card.style.display = "block";
      });
      return;
    }
  }
  allCards.forEach((card) => {
    let countryRegion = card.querySelector(".region .values").textContent;
    let countryName = card
      .querySelector(".country-name")
      .textContent.toLowerCase();

    if (countryRegion === filterValue && countryName.includes(searchValue)) {
      card.style.display = "block";
    } else {
      card.style.display = "none";
    }
  });
}

function searchCountry(searchText) {
  let searchValue = searchText.target.value.toLowerCase().trim();
  if (searchValue === sessionStorage.getItem("previousSearchValue")) {
    return;
  }
  if (searchValue) {
    sessionStorage.setItem("previousSearchValue", `${searchValue}`);
  } else {
    sessionStorage.setItem("previousSearchValue", "");
  }

  let allCards = Array.from(document.querySelectorAll(".card-body"));
  let filterValue = document.getElementById("filter-bar-select").value;
  console.log(filterValue);

  allCards.forEach((card) => {
    let countryName = card
      .querySelector(".country-name")
      .textContent.toLowerCase();
    let countryRegion = card.querySelector(".region .values").textContent;

    if (
      countryName.includes(searchValue) &&
      (filterValue.includes(countryRegion) || filterValue === "All")
    ) {
      card.style.display = "block";
    } else {
      card.style.display = "none";
    }
  });
}

function changetheme() {
  if (document.querySelector("#theme-mode").textContent === "Dark Mode") {
    document.querySelector("#theme-mode").textContent = "Light Mode";
    var list = Array.from(document.querySelectorAll(".theme-elements"));
    list.forEach((item) => {
      item.style.backgroundColor = "hsl(209, 23%, 22%)";
      item.style.color = "hsl(0, 0%, 100%)";
    });
    document.querySelector("body").style.backgroundColor = "hsl(207, 26%, 17%)";
    document.querySelector("body").style.color = "hsl(0, 0%, 100%)"
  } else {
    document.querySelector("#theme-mode").textContent = "Dark Mode";
    var list = Array.from(document.querySelectorAll(".theme-elements"));
    list.forEach((item) => {
      item.style.backgroundColor = "hsl(0, 0%, 100%)";
      item.style.color = "hsl(200, 15%, 8%)";
    });
    document.querySelector("body").style.backgroundColor = "hsl(0, 0%, 98%)";
    document.querySelector("body").style.color = "hsl(200, 15%, 8%)"
  }
}

async function server() {
  console.log("displayCards!");
  await fetchData();
  let data = JSON.parse(sessionStorage.getItem("countriesData"));
  displayCards(data);
}

async function fetchData() {
  if (sessionStorage["countriesData"]) {
    return;
  } else {
    try {
      const data = await (
        await fetch("https://restcountries.com/v3.1/all")
      ).json();
      countryCodes = data.reduce((acc, country) => {
        acc[country.cca3] = country.name.common;
        return acc;
      }, {});

      let refinedData = data.reduce((acc, country) => {
        let allInfo = {};

        allInfo.name = country.name.common;

        try {
          allInfo.nativeInfo = Object.values(
            Object.values(country.name.nativeName)[0]
          )[0];
        } catch (err) {
          allInfo.nativeInfo = "No native name found!";
        }

        allInfo.flag = country.flags.svg;

        allInfo.population = country["population"];

        allInfo.region = country["region"];

        allInfo.subregion = country["subregion"];

        try {
          allInfo.capital = country.capital[0];
        } catch (err) {
          allInfo.capital = "No Capital Found!";
        }

        allInfo.tld = country.tld;

        allInfo.currencies = Object.values(
          country.currencies || { name: "Not Found" }
        ).reduce((acc, currency) => {
          acc.push(currency.name);
          return acc;
        }, []);

        allInfo.languages = Object.values(
          country.languages || {} || ["Not Found"]
        );

        allInfo.borderCountries = (country.borders || [])
          .reduce((acc, countryCode) => {
            acc.push(countryCodes[countryCode]);
            return acc;
          }, [])
          .filter((country) => country);

        acc.push(allInfo);
        return acc;
      }, []);

      sessionStorage.setItem("countriesData", JSON.stringify(refinedData));
      return;
    } catch (err) {
      console.error(err);
    }
  }
}

async function displayCards(data) {
  document.querySelector("#temporary").style.display = "none";
  data.forEach((country) => {
    const newElement = document.createElement("div");
    newElement.innerHTML = `
    <div class="card-elements theme-elements">
      <a href="details.html">
        <img src=${country.flag} alt="">
        <div id="basic-info">
          <p class="country-name">${country.name}</p>
          <p class="population">Population: <span class="values">${country.population.toLocaleString(
            "en-US"
          )}</span></p>
          <p class="region">Region: <span class="values">${
            country.region
          }</span></p>
          <p class="capital">Capital: <span class="values">${
            country.capital
          }</span></p>
        </div>
      </a>
    </div>`;

    newElement.setAttribute("class", "card-body");
    document.querySelector("#cards-section").appendChild(newElement);
  });
}