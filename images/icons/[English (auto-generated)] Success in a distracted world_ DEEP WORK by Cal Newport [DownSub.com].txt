I recently read the book deep work by

author Cal Newport Cal defines deep work

as professional activities performed in

a state of distraction free

concentration that pushes your cognitive

abilities to their limit these efforts

create new value improve your skill and

are hard to replicate JK rowling use

deep work to complete the final book of

her Harry Potter series the deadly

hollows in 2007 she needed to escape the

distraction of screaming kids and

barking dogs so she checked into a suite

in a five-star hotel in downtown

Edinburgh Scotland she says I didn't

intend to stay there but the first days

writing went well so I kept coming back

and I ended up finishing the last of the

Harry Potter books here Bill Gates used

deep work in 1974 to program the first

version of basic in just eight weeks Cal

says that Gates worked with such

intensity for such lengths during the

two-month stretch that he would often

collapse into sleep on his keyboard in

the middle of writing a line of code he

would then sleep for an hour to wake up

and pick up right where he left off the

basic software that Gates wrote in eight

weeks well in a state of deep work

became the foundation of a billion

dollar company author Cal Newport an MIT

graduate and Georgetown professor claims

that deep work allowed him to double his

output of research papers while raising

a family writing this book and teaching

full-time at a prestigious university

the first two cases Gates and JK Rowling

might be extreme as most people don't

have the ability to go off the grid and

do deep work for weeks at a time but Cal

shows that we can maintain a busy

schedule and still find ways to do deep

work and produce significant results in

our lives that others find hard to

replicate but how exactly does deep work

lead to these best-selling books

innovative products and elite levels of

productivity while neuroscientists have

found that intense periods of focus in

isolated fields of work causes myelin to

develop in relevant areas of the brain

myelin is a white tissue that develops

around neurons and allows brain cells to

fire faster and cleaner so in a sense

when we practice deep

work we upgrade our brains and allow

specific brain circuits to fire more

effortlessly and effectively the brain

upgrade we get from deep work allows you

to rapidly connect ideas and uncover

creative solutions in today's economy

the ability to do deep work is

increasingly valuable an increasingly

rare it's valuable because when you

produce something great in our

hyper-connected world it has the ability

to spread to billions of people

producing something great is necessary

to stand out amongst the noise and avoid

being forgotten by the flood of

information that we deal with on a

day-to-day basis think of your last

tweet how long did it last how quickly

was it forgotten

deep work is becoming increasingly rare

because deep work requires undivided

attention and our world is being filled

with more and more tempting distractions

so the ability to do deep work is

becoming increasingly difficult

co-workers expect you to immediately

respond to an email or an instant

message employers want you to function

in an open office concept of constant

distraction your friends and followers

online expect you to maintain a social

media presence it's not enough to try to

ignore these distractions we are

hard-wired to be distracted and pay

attention to novelty in a 2012 study led

by psychologists Wilhelm Hofmann and Roy

Baumeister involving 205 adults found

that we are only able to resist

temptations to take a break from work to

check email surf the web or watch TV

just fifty percent of the time but there

is hope you can build a scale of deep

work and escape the trap of constant

distraction thus separating you from the

pack and making you indispensable in

today's economy here are three deep work

strategies that you can incorporate into

your schedule to heighten your ability

to focus and produce results that are

hard to replicate first schedule your

distraction periods at home and at work

most of us allow ourselves to go online

at any moment and check our phone

whenever buzzes or dings but doing so is

training your brain to avoid deep work a

day full of unscheduled distraction is

training your brain to give in to any

and all distractions to build your

tolerance to avoid distraction you need

to place boundaries on your distract

have a notepad nearby and put down the

next distraction break you'll have hold

your focus until that time at first it's

going to be painful but remember that

doing this is effectively doing the reps

that build your ability to concentrate

second develop a rhythmic deep work

ritual Cal says the easiest way to

consistently start deep work sessions is

to transform them into a simple regular

habit the goal in other words is to

generate a rhythm for this work that

removes the need for you to invest

energy in deciding if and when you're

going to go deep Cal uses several

examples in the book to show that

scheduling chunks of deep focus in an ad

hoc manner doesn't yield much

productivity at all for people who are

not seasoned at doing deep work it's

best for them to have a reoccurring time

each day or each week to go into deep

work early morning is typically the best

time to do this because at that time you

typically don't have to deal with

incoming requests the research shows

that people new to deep work can

typically only do it for about one hour

and masters of deep work can typically

only hold their attention up to four

hours in intervals between 60 and 90

minutes throughout the day so the

ultimate goal of each day is to plant

deep work rituals throughout the day

with the ultimate goal of building up

the some of your deep work practices to

four hours a day the third strategy to

cultivate deep work in your life is to

have a daily shutdown complete ritual

sleep is the price we need to pay in

order to do deep work it's the interest

we pay on the loans of intense focus

required to do deep work to ensure that

we get adequate sleep and restore our

attentional reserves for the following

day Cal recommends that we incorporate

an evening shutdown into our daily

routine an evening shutdown ritual

involves making a plan to complete any

unfinished tasks goals or projects the

following day getting a series of steps

lined out for the following day is

enough to get items off your mind so you

can disconnect for the rest of the day

when you get things off your mind you

restore the ability to sleep well and do

deep work the following day

after Cal completes his plan for the

following day he will say to himself

shutdown complete it's pretty cheesy but

he says it's a great cue to unplug in

the end deep work is incredibly valuable

because it changes your brain and allows

you to produce innovative work that is

hard to replicate that was the core

message that I gathered from Cal's great

book Cal does a great job of teaching

deep work rituals and reducing the fear

of leaving shallow work behind I highly

recommend this book for anyone looking

to make something meaningful in this

world if you would like a one-page PDF

summary that includes the steps needed

to build a deep work ritual just click

the link below and I'd be happy to email

it to you if you already subscribe to

the free productivity gain newsletter

this PDF is already in your inbox thanks

for watching

